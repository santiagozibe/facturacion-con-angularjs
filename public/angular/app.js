var app = angular.module("loginApp", 
// Dependenias de servicios
["login.loginService"]);

app.controller("mainCtrl", [
  "$scope", "LoginService", function ($scope, LoginService) {
   
   $scope.isInvalid = false;
   $scope.loading = false;
   $scope.loginMessage = "";

   $scope.userData = {};

   $scope.signIn = function (userData) {

      if (userData.usuario.length < 3 ){
         $scope.isInvalid = true;
         $scope.loginMessage = "El usuario debe tener al menos 3 caracter"
         return
      } else if (userData.password.length <= 4) {
         $scope.isInvalid = true;
         $scope.loginMessage = "Usuario o Contraseña incorrecta"
         return
      }

      $scope.isInvalid = false;
      $scope.loading = true;

      LoginService.login(userData).then(
         function (resp) {

            if (resp.err) {
               $scope.isInvalid = true;
               $scope.loading = false;
               $scope.loginMessage = resp.mensaje;
            } else {
               console.log(resp.mensaje);
               window.location = resp.url;
            }

         }
      )
   };
   

  }]);
