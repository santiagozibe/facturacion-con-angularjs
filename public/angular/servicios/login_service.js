var app = angular.module("login.loginService", []);

app.factory("LoginService", ["$http", "$q", function($http, $q) {

    var self = {

        login: function(userData) {
            var d = $q.defer();

            $http.post("php/login/post.verificar.php", userData).success(function(resp){

                console.log(resp);

                d.resolve(resp);

            });

            return d.promise;
        }

    }

    return self; 

}])