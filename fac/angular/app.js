var app = angular.module("facturacionApp", [
   "ngRoute",
   "jcs-autoValidate",
   // ! Inyeccion de los servivios creados
   "facturacionApp.configuracion",
   "facturacionApp.mensajes",
   "facturacionApp.notificaciones",
   "facturacionApp.clientes",
   // ! Inyeccion de controladores
   "facturacionApp.clientesCtrl",
   "facturacionApp.dashboardCtrl",
]);

// * mainCtrl => Disponible para toda la aplicacion

// Configuracion del idioma par las validaciones
angular.module('jcs-autoValidate')
.run([
    'defaultErrorMessageResolver',
    function (defaultErrorMessageResolver) {
        // To change the root resource file path
        defaultErrorMessageResolver.setI18nFileRootPath("angular/lib");
        defaultErrorMessageResolver.setCulture('es-co');
    }
]);

app.controller("mainCtrl", [
   "$scope",
   // ! Nombre de los servicios
   "Configuracion",
   "Mensajes",
   "Notificaciones",
   function ($scope, Configuracion, Mensajes, Notificaciones) {
      // * Variables para guardar los servivios
      $scope.config = {};
      $scope.mensajes = Mensajes.mensajes;
      $scope.notificaciones = Notificaciones.notificaciones;

      $scope.titulo = "";
      $scope.subtitulo = "";

      $scope.usuario = {
         nombre: "Santiago Zibecchi",
      };

      // * Consumiendo el servicio de configuracion.service
      Configuracion.cargar().then(
         function () {
            $scope.config = Configuracion.config;
            // console.log($scope.config);
         },
         function () {}
      );

      // * ==================================
      // * FUNCIONES GLOBALES del Scope
      // * ==================================

      $scope.isActive = function (menu, submenu, titulo, subtitulo) {
         $scope.titulo = titulo;
         $scope.subtitulo = subtitulo;

         $scope.mDashboard = "";
         $scope.mClientes = "";

         // Clase
         $scope[menu] = "active";
      };
   },
]);

// * ==================================
// * RUTAS
// * ==================================

// ! ng-view
app.config([
   "$routeProvider",
   function ($routeProvider) {
      $routeProvider
         .when("/", {
            templateUrl: "dashboard/dashboard.html",
            controller: "dashboardCtrl",
         })
         .when("/clientes/:pag", {
            templateUrl: "clientes/clientes.html",
            controller: "clientesCtrl",
         })
         .otherwise({
            redirectTo: "/",
         });
   },
]);

// * ==================================
// * FILTROS
// * ==================================

app.filter("quitarLetra", function () {
   return function (palabra) {
      if (palabra) {
         if (palabra.length) {
            return palabra.substr(1);
         } else {
            return palabra;
         }
      }
   };
}).filter("mensajeCorto", function () {
   return function (mensaje) {
      // Validar que exista el mensaje
      if (mensaje) {
         if (mensaje.length > 20) {
            return mensaje.substr(0, 35) + "...";
         } else {
            return mensaje;
         }
      }
   };
});
