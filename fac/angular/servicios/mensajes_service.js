var app = angular.module("facturacionApp.mensajes", []);

app.factory("Mensajes", [
   "$http",
   "$q",
   function ($http, $q) {
      var self = {
         mensajes: [
            {
               img: "dist/img/user2-160x160.jpg",
               nombre: "Juan Carlos",
               mensaje: "Bienvenido a nuestro servicio de facturacion",
               fecha: "5-Marzo",
            },
            {
               img: "dist/img/user7-128x128.jpg",
               nombre: "Mariana Soto",
               mensaje: "Bienvenido a nuestro servicio de facturacion",
               fecha: "5-Marzo",
            },
            {
               img: "dist/img/user1-128x128.jpg",
               nombre: "Victor Gomez",
               mensaje: "Bienvenido a nuestro servicio de facturacion",
               fecha: "14-Enero",
            },
            {
               img: "dist/img/user8-128x128.jpg",
               nombre: "Victor Gomez",
               mensaje: "Bienvenido a nuestro servicio de facturacion",
               fecha: "14-Enero",
            },
         ],
      };

      return self;
   },
]);
