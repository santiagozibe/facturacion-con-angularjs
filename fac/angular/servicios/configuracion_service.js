var app = angular.module("facturacionApp.configuracion", []);

app.factory("Configuracion", [
   "$http",
   "$q",
   function ($http, $q) {
      var self = {
         config: {},

         cargar: function () {
            var getJsonPromise = $q.defer();

            $http
               .get("configuracion.json")
               .success(function (data) {
                  self.config = data;
                  getJsonPromise.resolve();
               })
               .error(function () {
                  getJsonPromise.reject();
                  console.error(
                     "No se pudo cargar el archivo de configuracion"
                  );
               });

            return getJsonPromise.promise;
         },
      };

      return self;
   },
]);

// * Servicio para leer el JSON y por cada vez que se refresca el navegador lo lea. Si hay algun cambio, solo se cambia desde un sólo lugar
