var app = angular.module("facturacionApp.dashboardCtrl", []);

//  =======================================
//      CONTROLADOR DE DASHBOARD
//  =======================================

app.controller("dashboardCtrl", [
   "$scope",
   function ($scope) {
      $scope.isActive("mDashboard", "", "Dashboard", "Información");
   },
]);
