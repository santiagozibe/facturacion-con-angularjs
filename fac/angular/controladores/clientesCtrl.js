var app = angular.module("facturacionApp.clientesCtrl", []);

//  =======================================
//      CONTROLADOR DE CLIENTES
//  =======================================

app.controller("clientesCtrl", [
   // ! inyeccion de servicios (angular)
   "$scope",
   "$routeParams",
   // Servicios propios
   "Clientes",
   function ($scope, $routeParams, Clientes) {
      // pag se ecuentra definido en las rutas en app.js
      var pag = $routeParams.pag;

      $scope.isActive("mClientes", "", "Clientes", "Listado");

      $scope.clientes = {};
      $scope.clienteSelec = {};

      $scope.clientes = Clientes;

      $scope.moverA = function (pag) {
         Clientes.cargarPagina(pag).then(function () {
            $scope.clientes = Clientes;
         });
      };

      // Primera vez que se cargue
      // Funcion que reemplaza a href para evitar el refresco por defecto
      // de la pagina al traer data de otro paginado
      $scope.moverA(pag);

      // ====================================
      // Mostrar modal 
      // ====================================

      $scope.mostrarModal = function(cliente) {

         // Clono el cliente y lo guardo en una nueva variable
         angular.copy(cliente, $scope.clienteSelec);

         $("#modal_cliente").modal();

      };

      // Funcion para guardar los datos
      $scope.guardar = function(cliente, formCliente) {
         Clientes.guardar(cliente).then(function(){
            // codigo cuando se actualizo

            // JQuery
            $("#modal_cliente").modal("hide");
            $scope.clienteSelec = {};

            // Limpieza del formulario
            formCliente.autoValidateFormOptions.resetForm();
         })
      }


   },
]);
